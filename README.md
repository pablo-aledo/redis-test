# Helm charts

A list of helm charts for IPA deployments

# Chart Structure

This repository contains helm charts to be used in a Kubernetes deployment as a
way to install and configure instances of a IPA solution.

The structure of the repository follows the conventions of a standard HELM
chart repository. On top of that, some files can be used to ask the user for
extra information if the helm is deployed through the RancherOs manager.

Those two extra-files are `app-readme.md` (to present a summary of the
application) and `questions.yaml` to define questions to be prompted in the
Rancher Graphical-User-Interface.

The structure for each application is the following (for more details, see
[HELM developer reference](https://docs.helm.sh/developing_charts/) and
[RancherOs helm references](https://github.com/rancher/charts)).

```
charts/wordpress/<app version>/
  app-readme.md            # Rancher Specific: Readme file for display in Rancher 2.0 UI
  charts/                  # Directory containing dependency charts
  Chart.yaml               # Required Helm chart information file
  questions.yml            # Rancher Specific: File containing questions for Rancher 2.0 UI
  README.md                # Optional: Helm Readme file (will be rendered in Rancher 2.0 UI as well)
  requirements.yaml        # Optional YAML file listing dependencies for the chart
  templates/               # A directory of templates that, when combined with values.yml will generate K8s YAML
  values.yaml              # The default configuration values for this chart
```

